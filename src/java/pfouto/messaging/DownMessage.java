/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto.messaging;

import java.net.InetAddress;

public class DownMessage extends Message
{
    public final static int CODE = 2;

    private final int id;

    public DownMessage(int messageId, InetAddress from, int verb, long timestamp)
    {
        super(from, verb, CODE, timestamp);
        this.id = messageId;
    }

    public int getId()
    {
        return id;
    }

    @Override
    public String toString()
    {
        return super.toString() + " ID: " + id;
    }
}