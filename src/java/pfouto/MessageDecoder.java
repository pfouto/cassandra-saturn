/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto;

import java.net.InetAddress;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.apache.cassandra.net.CompactEndpointSerializationHelper;
import pfouto.messaging.DownMessage;

public class MessageDecoder extends ByteToMessageDecoder {

    private static final Logger logger = LoggerFactory.getLogger(MessageDecoder.class);

    private int size = -1;

    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        ByteBufInputStream inputStream = new ByteBufInputStream(in);
        try {

            if (size == -1 && in.readableBytes() >= 4) {
                size = in.readInt();
            }

            if (size != -1 && in.readableBytes() >= size) {
                //code
                int messageCode = in.readInt();
                //from
                InetAddress from = CompactEndpointSerializationHelper.deserialize(inputStream);
                //verb
                int verb = in.readInt();
                //timestamp
                long timestamp = in.readLong();
                //specific stuff
                switch (messageCode) {
                    case DownMessage.CODE:
                        int id = in.readInt();
                        out.add(new DownMessage(id, from, verb, timestamp));
                        break;
                    default:
                        throw new Exception("unknown/unhandled messageType: " + messageCode);
                }
                size = -1;
            }

        } catch (Exception e) {
            logger.error("Error Decoding message: " + e.getMessage());
        }
    }


}

